﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Services;
using System.IdentityModel.Tokens;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Caseowary
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void RefreshValidationSettings()
        {

            string configPath = AppDomain.CurrentDomain.BaseDirectory + "\\" + "Web.config";
            var tenant = ConfigurationManager.AppSettings["TenantId"];
            string metadataAddress = string.Format("https://login.windows.net/{0}/FederationMetadata/2007-06/FederationMetadata.xml", tenant);

            // Alternative implementation is to extract and add thumbs on the fly
            // see IdentityConfig.cs, method AddfederationThumbs
            ValidatingIssuerNameRegistry.WriteToConfig(metadataAddress, configPath);
        }

        protected void Application_Start()
        {
            IdentityConfig.ConfigureIdentity();
            
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            RefreshValidationSettings();
        }

        void WSFederationAuthenticationModule_RedirectingToIdentityProvider(object sender, RedirectingToIdentityProviderEventArgs e)
        {
            if (!String.IsNullOrEmpty(IdentityConfig.Realm))
            {
                e.SignInRequestMessage.Realm = IdentityConfig.Realm;
            }
        }
    }
}
