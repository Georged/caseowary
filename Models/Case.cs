﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Caseowary.Models
{
    public enum CaseStatusCode
    {
        ProblemSolved = 5,
        InformationProvided = 1000,
        Canceled = 6,
        InProgress = 1,
        OnHold = 2,
        WaitingforDetails = 3,
        Researching = 4
    }

    public class CaseModel
    {
        public Guid PrimaryKey { get; set; }
        public string Title { get; set; }
        public string Number { get; set; }
        public CaseStatusCode Status { get; set; }

        public string StatusName
        {
            get
            {
                return Status.ToString();
            }
        }
    }

    public class CaseViewModel
    {
        public IEnumerable<CaseModel> Cases;
        public string Username;
        public Guid UserId;
    }
}