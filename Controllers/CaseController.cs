﻿using Caseowary.Models;
using Caseowary.Utils;
using Caseowary.Xrm;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace Caseowary.Controllers
{
    public class CaseController : Controller
    {
        // GET: /Case/
        public ActionResult Index()
        {
            IdentityConfig ic = IdentityConfig.Current;
            if (ic.IsAuthenticated)
            {
                Guid userId = Helper.GetUserId(ic.TokenResult.AccessToken);
                ViewBag.Username = Helper.GetUserName(ic.TokenResult.AccessToken, userId);

                var service = Helper.GetODataService(ic.TokenResult.AccessToken);
                var cases = service.IncidentSet
                    .Where( c => c.OwnerId.Id == userId)
                    .Select(c => 
                        new CaseModel() 
                        { 
                            PrimaryKey = c.IncidentId, 
                            Title = c.Title, 
                            Number = c.TicketNumber, 
                            Status = (CaseStatusCode)c.StatusCode.Value.GetValueOrDefault(1) 
                        })
                    .ToList();
                
                return View(cases);
            }
            else
            {
                Session["ReturnUrl"] = Request.Url.AbsoluteUri;
                return new RedirectResult(ic.GetAuthorizationUrl());
            }
        }

        // GET: /Case/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Case/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Case/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Case/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Case/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Case/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Case/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
