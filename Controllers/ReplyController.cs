﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using Caseowary.Utils;
using System.Configuration;

namespace Caseowary.Controllers
{
    public class ReplyController : Controller
    {
        //
        // GET: /Reply/
        public ActionResult Index(string code)
        {
            IdentityConfig.Current.AcquireToken(code);
            var url = "/";
            if (Session["ReturnUrl"] != null)
            {
                url = (string)Session["ReturnUrl"];
                Session["ReturnUrl"] = null;
            }
            return new RedirectResult(url);
        }
	}
}