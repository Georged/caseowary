﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace Caseowary.Utils
{
    public static class HttpRequestBuilder
    {
        public static string WhoAmIRequest(string accessToken)
        {
            // Default SOAP envelope string. This XML code was obtained using the SOAPLogger tool.
            string xmlSOAP =
                @"<s:Envelope xmlns:s='http://schemas.xmlsoap.org/soap/envelope/'>
                    <s:Body>
                    <Execute xmlns='http://schemas.microsoft.com/xrm/2011/Contracts/Services' xmlns:i='http://www.w3.org/2001/XMLSchema-instance'>
                        <request i:type='b:WhoAmIRequest' xmlns:a='http://schemas.microsoft.com/xrm/2011/Contracts' xmlns:b='http://schemas.microsoft.com/crm/2011/Contracts'>
                        <a:Parameters xmlns:c='http://schemas.datacontract.org/2004/07/System.Collections.Generic' />
                        <a:RequestId i:nil='true' />
                        <a:RequestName>WhoAmI</a:RequestName>
                        </request>
                    </Execute>
                    </s:Body>
                </s:Envelope>";

            // The URL for the SOAP endpoint of the organization web service.
            string url = ConfigurationManager.AppSettings["OrgSvcUrl"];
            
            // Use the RetrieveMultiple CRM message as the SOAP action.
            string SOAPAction = "http://schemas.microsoft.com/xrm/2011/Contracts/Services/IOrganizationService/Execute";

            // Create a new HTTP request.
            HttpClient httpClient = new HttpClient();

            // Set the HTTP authorization header using the access token.
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            // Finish setting up the HTTP request.
            HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Post, url);
            req.Headers.Add("SOAPAction", SOAPAction);
            req.Content = new StringContent(xmlSOAP);
            req.Content.Headers.ContentType = MediaTypeHeaderValue.Parse("text/xml; charset=utf-8");

            // Send the request asychronously and wait for the response.
            using (HttpResponseMessage response = httpClient.SendAsync(req).Result)
            {
                return response.Content.ReadAsStringAsync().Result;
            }

        }
    }
}